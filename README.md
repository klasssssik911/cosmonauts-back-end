# Cosmonauts test project (Back-end)

## Build Setup

### Ruby on Rails
* install ruby
* install rails
http://railsapps.github.io/installing-rails.html

### Setup DB
* create cosmonauts table
* create cosmonauts_test table
* copy config/example.database.yml and rename to config/database.yml
* change username, password, host and port to your

``` bash
# install dependencies
bundle install

# create DB tables
rake db:migrate
rake db:migrate RAILS_ENV=test

# create default data
rake db:seed

# run server
rails s

# run unit tests
bundle exec rspec
```