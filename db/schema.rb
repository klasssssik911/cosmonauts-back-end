# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170617184346) do

  create_table "cosmonauts", force: :cascade do |t|
    t.string   "first_name",      limit: 255
    t.string   "last_name",       limit: 255
    t.date     "dob"
    t.boolean  "gender"
    t.boolean  "is_verified"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "email",           limit: 255
    t.string   "phone",           limit: 255
    t.string   "personal_number", limit: 255
    t.integer  "user_id",         limit: 4
  end

  add_index "cosmonauts", ["user_id"], name: "index_cosmonauts_on_user_id", using: :btree

  create_table "superpowers", force: :cascade do |t|
    t.string   "title",        limit: 255
    t.integer  "cosmonaut_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "superpowers", ["cosmonaut_id"], name: "index_superpowers_on_cosmonaut_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "email",           limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_foreign_key "cosmonauts", "users"
  add_foreign_key "superpowers", "cosmonauts"
end
