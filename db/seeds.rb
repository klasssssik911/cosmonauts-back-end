# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.destroy_all
admin = User.create!(name: 'Admin', email: 'klassik911@ukr.net', :password => 'password')

Cosmonaut.destroy_all
50.times do
  cosmonaut = Cosmonaut.new(
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name,
      email: Faker::Internet.email,
      phone: Faker::PhoneNumber.cell_phone,
      dob: Faker::Date.birthday(35, 65),
      gender: Faker::Boolean.boolean,
      is_verified: Faker::Boolean.boolean,
      personal_number: Faker::StarWars.droid,
      user: admin
  )
  if cosmonaut.save
    rand(1..5).times do
      cosmonaut.superpowers.create(title: Faker::Space.galaxy)
    end
  end
end