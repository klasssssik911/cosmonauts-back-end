class CreateSuperpowers < ActiveRecord::Migration
  def change
    create_table :superpowers do |t|
      t.string :title
      t.references :cosmonaut, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
