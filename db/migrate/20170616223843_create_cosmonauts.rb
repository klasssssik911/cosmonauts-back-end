class CreateCosmonauts < ActiveRecord::Migration
  def change
    create_table :cosmonauts do |t|
      t.string :first_name
      t.string :last_name
      t.date :dob
      t.boolean :gender
      t.boolean :is_verified
      t.string :created_by

      t.timestamps null: false
    end
  end
end
