class AssociateUsersWithCosmonauts < ActiveRecord::Migration
  def up
    add_reference :cosmonauts, :user, :index => true
    add_foreign_key :cosmonauts, :users
    if column_exists? :cosmonauts, :created_by
      remove_column :cosmonauts, :created_by
    end
  end

  def down
    remove_foreign_key :cosmonauts, { :column => :user_id }
    remove_reference :cosmonauts, :user, index: true
  end
end
