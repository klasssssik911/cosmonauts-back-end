class AddNumberToCosmonauts < ActiveRecord::Migration
  def change
    add_column :cosmonauts, :personal_number, :string
  end
end
