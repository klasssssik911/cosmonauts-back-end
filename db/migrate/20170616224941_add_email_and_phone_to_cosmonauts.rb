class AddEmailAndPhoneToCosmonauts < ActiveRecord::Migration
  def change
    add_column :cosmonauts, :email, :string
    add_column :cosmonauts, :phone, :string
  end
end
