class CosmonautSerializer < ActiveModel::Serializer
  # attributes to be serialized
  attributes :id, :first_name, :last_name, :email, :phone, :dob, :gender,
             :is_verified, :personal_number, :created_at, :updated_at, :user
  # model association
  has_many :superpowers
end
