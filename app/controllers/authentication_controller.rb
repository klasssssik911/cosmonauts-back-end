class AuthenticationController < ApplicationController
  skip_before_action :authorize_request, only: :authenticate
  # return auth token once user is authenticated
  def authenticate
    begin
      current_user = nil
      auth_token = AuthenticateUser.new(
          auth_params[:email],
          auth_params[:password]
      ).call
      if auth_token
        request.headers['Authorization'] = auth_token
        current_user = (AuthorizeApiRequest.new(request.headers).call)[:user]
      end
      json_response(auth_token: auth_token, user: current_user)
    rescue => ex
      json_response(message: ex.message, error: true)
    end
  end

  private

  def auth_params
    params[:auth].permit(:email, :password)
  end
end
