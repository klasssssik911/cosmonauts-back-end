class UsersController < ApplicationController
  skip_before_action :authorize_request, only: :create
  # POST /signup
  # return authenticated token upon signup
  def create
    user = User.new(user_params)
    if user.save
      auth_token = AuthenticateUser.new(user.email, user.password).call
      response = { message: Message.account_created, auth_token: auth_token }
      json_response(response, :created)
    else
      json_response(message: user.errors.full_messages.first, error: true)
    end
  end

  def current
    json_response(
        {
            user: {
                id: current_user.id,
                name: current_user.name
            }
        }
    )
  end

  def update
    if current_user.update_attributes(user_update_params)
      current
    else
      json_response(message: user.errors.full_messages.first, error: true)
    end
  end

  def update_password
    begin
      user = User.find_by(email: current_user.email)
      if user && user.authenticate(params[:data][:current])
        if is_valid_new_password(params[:data][:new_password], params[:data][:password_confirmation])
          if user.update_attribute(:password, params[:data][:new_password])
            json_response({success: true})
          else
            json_response(message: user.errors.full_messages.first, error: true)
          end
        else
          json_response({message: 'Nove heslo nebo heslo pro potvrzení je nesprávné', error: true})
        end
      else
        json_response({message: 'Chybné aktuální heslo', error: true})
      end
    rescue => ex
      json_response({message: ex.message, error: true})
    end
  end

  private

  def is_valid_new_password(new, confirmation)
    !new.blank? && !confirmation.blank? &&
        new.to_s.size >= 6 && new.to_s == confirmation.to_s
  end

  def user_params
    params.permit(
        :name,
        :email,
        :password,
        :password_confirmation
    )
  end

  def user_update_params
    params[:data].permit(:name)
  end

end
