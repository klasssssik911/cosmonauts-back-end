module V1
  class SuperpowersController < ApplicationController
    before_action :set_cosmonaut
    before_action :set_cosmonaut_superpower, only: [:show, :update, :destroy]

    # GET /cosmonauts/:cosmonaut_id/superpowers
    def index
      json_response(@cosmonaut.superpowers)
    end

    # GET /cosmonauts/:cosmonaut_id/superpowers/:id
    def show
      json_response(@superpower)
    end

    # POST /cosmonauts/:cosmonaut_id/superpowers
    def create
      if @cosmonaut.superpowers.create!(superpower_params)
        json_response(@cosmonaut, :created)
      else
        json_response(message: @cosmonaut.errors.full_messages.first, error: true)
      end
    end

    # PUT /cosmonauts/:cosmonaut_id/superpowers/:id
    def update
      if @superpower.update_attributes(superpower_params)
        json_response(@superpower)
      else
        json_response(message: @superpower.errors.full_messages.first, error: true)
      end
    end

    # DELETE /cosmonauts/:cosmonaut_id/superpowers/:id
    def destroy
      if @superpower.destroy
        json_response(success: true)
      else
        json_response(message: @superpower.errors.full_messages.first, error: true)
      end
    end

    private

    def superpower_params
      params[:superpower].permit(:title)
    end

    def set_cosmonaut
      @cosmonaut = Cosmonaut.find(params[:cosmonaut_id])
    end

    def set_cosmonaut_superpower
      @superpower = @cosmonaut.superpowers.find_by!(id: params[:id]) if @cosmonaut
    end
  end

end
