module V1
  class CosmonautsController < ApplicationController
    before_action :set_cosmonaut, only: [:show, :update, :destroy]

    # GET /cosmonauts
    def index
      page = params[:page] || 1
      limit = params[:limit] || 10
      count = @current_user.cosmonauts.count
      order = params[:order].to_s.strip
      if order.size > 0
        if order[0] == '-'
          order = "#{order[1..order.size]} desc"
        else
          order = "#{order} asc"
        end
      else
        order = 'created_at desc'
      end
      @cosmonauts = @current_user.cosmonauts
                        .ransack(params)
                        .result(distinct: true)
                        .order(order)
                        .paginate(page: page, per_page: limit).as_json
      superpowers = Superpower.where(:cosmonaut_id => @cosmonauts.map{|x| x['id']}).as_json
      @cosmonauts.each do |cosmonaut|
        cosmonaut['superpowers'] = superpowers.select{|x| x['cosmonaut_id'] == cosmonaut['id']}
      end
      json_response(
          {
              items: @cosmonauts,
              count: @cosmonauts.size,
              total: count
          }
      )
    end

    # POST /cosmonauts
    def create
      @cosmonaut = @current_user.cosmonauts.new(cosmonaut_params)
      if @cosmonaut.save
        save_superpowers
        json_response(@cosmonaut, :created)
      else
        json_response(message: @cosmonaut.errors.full_messages.first, error: true)
      end
    end

    # GET /cosmonauts/:id
    def show
      json_response(@cosmonaut)
    end

    # PUT /cosmonauts/:id
    def update
      if @cosmonaut.update_attributes(cosmonaut_params)
        save_superpowers
        json_response(@cosmonaut)
      else
        json_response(message: @cosmonaut.errors.full_messages.first, error: true)
      end
    end

    # DELETE /cosmonauts/:id
    def destroy
      if @cosmonaut
        if @cosmonaut.destroy
          json_response(success: true)
        else
          json_response(message: @cosmonaut.errors.full_messages.first, error: true)
        end
      else
        json_response(message: 'Cosmonaut does not exist', error: true)
      end
    end

    # DELETE /cosmonauts/destroy-list
    def destroy_list
      result = {success: true}
      if params[:ids] && params[:ids].kind_of?(Array) && params[:ids].size > 0
        unless @current_user.cosmonauts.where(:id => params[:ids]).destroy_all
          result[:success] = false
        end
      end
      json_response(result)
    end

    private

    def cosmonaut_params
      # whitelist params
      params[:cosmonaut][:gender] = params[:cosmonaut][:gender].to_i
      params[:cosmonaut].permit(:first_name, :last_name, :email, :phone, :dob, :gender, :personal_number)
    end

    def set_cosmonaut
      @cosmonaut = @current_user.cosmonauts.find(params[:id])
    end

    def save_superpowers
      if params[:cosmonaut][:superpowers] && params[:cosmonaut][:superpowers].kind_of?(Array) && params[:cosmonaut][:superpowers].size > 0
        ids = []
        params[:cosmonaut][:superpowers].each do |superpower|
          if superpower[:id].nil?
            new_superpower = @cosmonaut.superpowers.new({title: superpower[:title]})
            ids << new_superpower.id if new_superpower.save
          else
            edited_superpower = Superpower.find_by_cosmonaut_id_and_id(@cosmonaut.id, superpower[:id])
            if edited_superpower and edited_superpower.update_attribute(:title, superpower[:title])
              ids << edited_superpower.id
            end
          end
        end
        if ids.size > 0
          @cosmonaut.superpowers.where('superpowers.id not in (?)', ids).destroy_all
        else
          @cosmonaut.superpowers.destroy_all
        end
      end
    end
  end

end
