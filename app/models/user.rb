class User < ActiveRecord::Base
  # encrypt password
  has_secure_password

  # Model associations
  has_many :cosmonauts, foreign_key: :user_id, :dependent => :destroy

  # Validations
  validates_presence_of :name, :email, :password_digest
  validates_uniqueness_of :email

  validates_format_of :name, :with => /\A[^0-9`!@#\$%\^&*+_=]+\z/
  validates :email, format: {:with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "Doesn't match with email address."}
end
