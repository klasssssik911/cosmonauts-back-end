class Superpower < ActiveRecord::Base
  # model association
  belongs_to :cosmonaut

  # validation
  validates_presence_of :title
end