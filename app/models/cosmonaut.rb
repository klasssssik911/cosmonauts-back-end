class Cosmonaut < ActiveRecord::Base
  belongs_to :user

  # model association
  has_many :superpowers, dependent: :destroy

  # validations
  validates_presence_of :first_name, :last_name, :email, :personal_number
  validates :first_name, format: { with: /\A[a-zA-Z \-']+\z/, message: 'For First name only allows letters.' }
  validates :last_name, format: { with: /\A[a-zA-Z \-']+\z/, message: 'Last Last name only allows letters.' }
  validates :email, format: {:with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "Doesn't match with email address."}

  def self.search_text(search)
    if search and search.to_s.strip.size > 0
      wildcard = "%#{search.to_s.strip}%"
      where('cosmonauts.first_name like :search OR
            cosmonauts.last_name like :search OR
            cosmonauts.email like :search OR
            cosmonauts.phone like :search OR
            cosmonauts.personal_number like :search', search: wildcard)
    else
      all
    end
  end

  def self.ransackable_scopes(auth_object = nil)
    %i(search_text)
  end
end
