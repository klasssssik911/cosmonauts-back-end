require 'rails_helper'
require 'faker'
RSpec.describe 'Cosmonauts API', type: :request do
  # initialize test data
  let(:user) { create(:user) }
  let!(:cosmonauts) { create_list(:cosmonaut, 10, user_id: user.id) }
  let(:cosmonaut_id) { cosmonauts.first.id }
  # authorize request
  let(:headers) { valid_headers }

  # Test suite for GET /cosmonauts
  describe 'GET /cosmonauts' do
    before { get '/cosmonauts', {}, headers }

    it 'returns cosmonauts' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json['items'].size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /cosmonauts/:id
  describe 'GET /cosmonauts/:id' do
    before { get "/cosmonauts/#{cosmonaut_id}", {}, headers }

    context 'when the record exists' do
      it 'returns the cosmonaut' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(cosmonaut_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:cosmonaut_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Cosmonaut/)
      end
    end
  end

  # Test suite for POST /cosmonauts
  describe 'POST /cosmonauts' do
    # valid payload
    let(:params) do
      {
          :cosmonaut => {
              :first_name => 'Denys',
              :last_name => 'Sobko',
              :email => 'denys.sobko@gmail.com',
              :phone => '(111) 111-11-11',
              :dob => 25.years.ago,
              :gender => 1,
              :personal_number => '1DS89555-5'
          }
      }
    end

    context 'when the request is valid' do
      before { post '/cosmonauts', params.to_json, headers }

      it 'creates a cosmonaut' do
        expect(json['email']).to eq('denys.sobko@gmail.com')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before do
        params[:cosmonaut][:email] = nil
        post '/cosmonauts', params.to_json, headers
      end

      it 'returns a validation failure message' do
        expect(json['error']).to eq(true)
      end
    end
  end

  # Test suite for PUT /cosmonauts/:id
  describe 'PUT /cosmonauts/:id' do
    let(:params) {
      {
          :cosmonaut => {
              :email => Faker::Internet.email
          }
      }.to_json
    }

    context 'when the record exists' do
      before { put "/cosmonauts/#{cosmonaut_id}", params, headers }

      it 'updates the record' do
        expect(json['id']).to eq(cosmonaut_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  # Test suite for DELETE /cosmonauts/:id
  describe 'DELETE /cosmonauts/:id' do
    before { delete "/cosmonauts/#{cosmonaut_id}", {}, headers }

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
end
