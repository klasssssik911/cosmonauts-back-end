require 'rails_helper'

RSpec.describe 'Superpowers API' do
  # Initialize the test data
  let(:user) { create(:user) }
  let!(:cosmonaut) { create(:cosmonaut, user_id: user.id) }
  let!(:superpowers) { create_list(:superpower, 20, cosmonaut_id: cosmonaut.id) }
  let(:cosmonaut_id) { cosmonaut.id }
  let(:id) { superpowers.first.id }
  let(:headers) { valid_headers }

  # Test suite for GET /cosmonauts/:cosmonaut_id/superpowers
  describe 'GET /cosmonauts/:cosmonaut_id/superpowers' do
    before { get "/cosmonauts/#{cosmonaut_id}/superpowers", {}, headers }

    context 'when cosmonaut exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all cosmonaut superpowers' do
        expect(json.size).to eq(20)
      end
    end

    context 'when cosmonaut does not exist' do
      let(:cosmonaut_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Cosmonaut/)
      end
    end
  end

  # Test suite for GET /cosmonauts/:cosmonaut_id/superpowers/:id
  describe 'GET /cosmonauts/:cosmonaut_id/superpowers/:id' do
    before { get "/cosmonauts/#{cosmonaut_id}/superpowers/#{id}", {}, headers }

    context 'when cosmonaut superpower exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the superpower' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when cosmonaut superpower does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Superpower/)
      end
    end
  end

  # Test suite for POST /cosmonauts/:cosmonaut_id/superpowers
  describe 'POST /cosmonauts/:cosmonaut_id/superpowers' do
    let(:valid_attributes) { { superpower: { title: 'Test Superpower 1' }}.to_json }

    context 'when request attributes are valid' do
      before { post "/cosmonauts/#{cosmonaut_id}/superpowers", valid_attributes, headers }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/cosmonauts/#{cosmonaut_id}/superpowers", { superpower: { title: nil }}.to_json, headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Title can't be blank/)
      end
    end
  end

  # Test suite for PUT /cosmonauts/:cosmonaut_id/superpowers/:id
  describe 'PUT /cosmonauts/:cosmonaut_id/superpowers/:id' do
    let(:valid_attributes) { { title: 'Updated Superpower 1' }.to_json }

    before { put "/cosmonauts/#{cosmonaut_id}/superpowers/#{id}", valid_attributes, headers }

    context 'when superpower exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'updates the superpower' do
        updated_superpower = Superpower.find(id)
        expect(updated_superpower.title).to match(/Updated Superpower 1/)
      end
    end

    context 'when the superpower does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Superpower/)
      end
    end
  end

  # Test suite for DELETE /cosmonauts/:cosmonaut_id/superpowers/:id
  describe 'DELETE /cosmonauts/:cosmonaut_id/superpowers/:id' do
    before { delete "/cosmonauts/#{cosmonaut_id}/superpowers/#{id}", {}, headers }

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
end