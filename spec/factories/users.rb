require 'faker'

FactoryGirl.define do
  factory :user do
    name { Faker::Name.name }
    email 'test@user.com'
    password 'password'
  end
end