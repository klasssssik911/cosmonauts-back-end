require 'faker'
FactoryGirl.define do
  factory :cosmonaut do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    phone { Faker::PhoneNumber.cell_phone }
    dob { Faker::Date.birthday(35, 65) }
    gender true
    is_verified { Faker::Boolean.boolean }
    personal_number { Faker::StarWars.droid }
    user_id nil
  end
end