FactoryGirl.define do
  factory :superpower do
    title { Faker::Space.galaxy }
    cosmonaut_id nil
  end
end
