require 'rails_helper'

RSpec.describe Superpower, type: :model do
  # Association test
  # ensure an superpower record belongs to a single cosmonaut record
  it { should belong_to(:cosmonaut) }
  # Validation test
  # ensure column title is present before saving
  it { should validate_presence_of(:title) }
end
