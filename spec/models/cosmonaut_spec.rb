require 'rails_helper'

RSpec.describe Cosmonaut, type: :model do
  # Association test
  # ensure Cosmonaut model has a 1:m relationship with the Superpower model
  it { should have_many(:superpowers).dependent(:destroy) }
  # Validation tests
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should validate_presence_of(:personal_number) }
  it { should validate_presence_of(:email) }
end
