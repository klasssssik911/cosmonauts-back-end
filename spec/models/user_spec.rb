require 'rails_helper'

# Test suite for User model
RSpec.describe User, type: :model do
  # Validation tests
  # ensure User model has a 1:m relationship with the Cosmonaut model
  it { should have_many(:cosmonauts).dependent(:destroy) }
  # ensure name, email and password_digest are present before save
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password_digest) }
  it { should validate_uniqueness_of(:email) }
end
